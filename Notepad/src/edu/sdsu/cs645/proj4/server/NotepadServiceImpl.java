package edu.sdsu.cs645.proj4.server;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.util.ArrayList;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpSession;

import com.google.gwt.user.server.rpc.RemoteServiceServlet;

import edu.sdsu.cs645.proj4.client.NotepadService;
import edu.sdsu.cs645.proj4.shared.StatusCode;

/**
 * The server-side implementation of the RPC service.
 */
@SuppressWarnings("serial")
public class NotepadServiceImpl extends RemoteServiceServlet implements
		NotepadService {

	@Override
	public String load(String path, String filename)
			throws IllegalArgumentException {
		// quit if unauthorized
		if (!this.isAuthorized())
			return null;
		// get file path
		HttpSession session = this.getThreadLocalRequest().getSession(false);
		String username = (String) session.getAttribute("username");
		filename = getServletContext().getRealPath("/") + "/data/" + username
				+ path + filename + ".txt";
		// check if file exists, otherwise return empty string
		File file = new File(filename);
		if (!file.exists()) {
			return "";
		}
		// return content stored in file
		StringBuilder sb = new StringBuilder();
		try {
			BufferedReader in = new BufferedReader(new FileReader(filename));
			String line;
			while ((line = in.readLine()) != null) {
				sb.append(line);
			}
			in.close();
		} catch (Exception ex) {
			sb.append(ex.getMessage());
		}
		return sb.toString();
	}

	@Override
	public void store(String path, String filename, String content)
			throws IllegalArgumentException {
		// quit if unauthorized
		if (!this.isAuthorized())
			return;
		// get file path
		HttpSession session = this.getThreadLocalRequest().getSession(false);
		String username = (String) session.getAttribute("username");
		filename = getServletContext().getRealPath("/") + "/data/" + username
				+ path + filename + ".txt";
		// write to file
		try {
			PrintWriter out = new PrintWriter(new FileWriter(filename));
			out.print(content.replaceAll("\r\n|\n", "<br />"));
			out.close();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	@Override
	public Boolean login(String username, String password)
			throws IllegalArgumentException {
		if (EncryptionUtilities.isValid(username, password)) {

			HttpSession session = this.getThreadLocalRequest().getSession(true);

			session.setAttribute("username", username);
			session.setMaxInactiveInterval(60 * 60 * 24 * 14); // 2 weeks

			Cookie cookie = new Cookie("username", username);
			cookie.setMaxAge(60 * 60 * 24 * 14); // 2 weeks
			this.getThreadLocalResponse().addCookie(cookie);

			return true;
		}
		return false;
	}

	@Override
	public Boolean isAuthorized() throws IllegalArgumentException {
		// compare cookie and session - true if username value in both match
		HttpSession session = this.getThreadLocalRequest().getSession(false);
		if (session != null) {
			String username = (String) session.getAttribute("username");
			if (username != null) {
				Cookie[] cookies = this.getThreadLocalRequest().getCookies();
				if (cookies != null) {
					for (Cookie cookie : cookies) {
						if (cookie.getName().equals("username")) {
							return (cookie.getValue().equals(username));
						}
					}
				}
			}
		}
		return false;
	}

	@Override
	public ArrayList<String> getFiles(String path)
			throws IllegalArgumentException {
		// quit if unauthorized
		if (!this.isAuthorized())
			return null;
		// get file path
		HttpSession session = this.getThreadLocalRequest().getSession(false);
		String username = (String) session.getAttribute("username");
		ArrayList<String> files = new ArrayList<String>();
		final File folder = new File(getServletContext().getRealPath("/")
				+ "/data/" + username + path);
		if (!folder.exists()) {
			if (!folder.mkdir()) {
				System.out.println("Failed to create " + folder.getName());
			}
		}
		for (final File file : folder.listFiles()) {
			if (file.isDirectory()) {
				continue;
			} else {
				// I don't want file extension
				files.add(file.getName().substring(0,
						file.getName().lastIndexOf('.')));
			}
		}
		return files;
	}

	@Override
	public ArrayList<String> getFolders(String path)
			throws IllegalArgumentException {
		// quit if unauthorized
		if (!this.isAuthorized())
			return null;
		// get file path
		HttpSession session = this.getThreadLocalRequest().getSession(false);
		String username = (String) session.getAttribute("username");
		ArrayList<String> folders = new ArrayList<String>();
		final File folder = new File(getServletContext().getRealPath("/")
				+ "/data/" + username + path);
		if (!folder.exists()) {
			if (!folder.mkdir()) {
				System.out.println("Failed to create " + folder.getName());
			}
		}
		for (final File file : folder.listFiles()) {
			if (!file.isDirectory()) {
				continue;
			} else {
				folders.add(file.getName());
			}
		}
		return folders;
	}

	@Override
	public Integer addFile(String path, String filename)
			throws IllegalArgumentException {
		// quit if unauthorized
		if (!this.isAuthorized())
			return StatusCode.UNAUTHORIZED;
		// get file path
		HttpSession session = this.getThreadLocalRequest().getSession(false);
		String username = (String) session.getAttribute("username");
		String folderPath = getServletContext().getRealPath("/") + "/data/"
				+ username + path;
		// create folder path if non-existent
		File folder = new File(folderPath);
		if (!folder.exists()) {
			if (!folder.mkdir()) {
				return StatusCode.FAILED;
			}
		}
		// get full file path
		filename = folderPath + filename + ".txt";
		// write to file
		try {
			PrintWriter out = new PrintWriter(new FileWriter(filename));
			out.print("");
			out.close();
		} catch (Exception ex) {
			ex.printStackTrace();
			return StatusCode.FAILED;
		}
		return StatusCode.SUCCESS;
	}

	@Override
	public Integer addFolder(String path, String foldername)
			throws IllegalArgumentException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Integer deleteFile(String path, String filename)
			throws IllegalArgumentException {
		// quit if unauthorized
		if (!this.isAuthorized())
			return StatusCode.UNAUTHORIZED;
		// get file path
		HttpSession session = this.getThreadLocalRequest().getSession(false);
		String username = (String) session.getAttribute("username");
		String folderPath = getServletContext().getRealPath("/") + "/data/"
				+ username + path;
		// get full file path
		filename = folderPath + filename + ".txt";
		File file = new File(filename);
		if (file.delete()) {
			return StatusCode.SUCCESS;
		} else {
			return StatusCode.FAILED;
		}
	}

	@Override
	public Boolean logout() throws IllegalArgumentException {
		try {
			HttpSession session = this.getThreadLocalRequest()
					.getSession(false);
			session.removeAttribute("username");

			Cookie[] cookies = this.getThreadLocalRequest().getCookies();
			if (cookies != null) {
				for (Cookie cookie : cookies) {
					if (cookie.getName().equals("username")) {
						cookie.setMaxAge(-1);
						cookie.setValue("");
						this.getThreadLocalResponse().addCookie(cookie);
					}
				}
			}
			return true;
		} catch (Exception ex) {
			System.out.println("GWT019: error at logout:");
			ex.printStackTrace();
			return false;
		}
	}
}
