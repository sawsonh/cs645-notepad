package edu.sdsu.cs645.proj4.shared;

public class Conversion {

	public static String toHtml(String text) {
		return text.replaceAll("\r\n|\n", "<br />");
	}
}
