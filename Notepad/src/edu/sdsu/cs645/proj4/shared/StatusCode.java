package edu.sdsu.cs645.proj4.shared;

public class StatusCode {

	public final static int SUCCESS = 0;
	public final static int FAILED = 1;
	public final static int UNAUTHORIZED = 2;
	public final static int FILE_EXISTS = 3;
	public final static int FOLDER_EXISTS = 4;
	
}
