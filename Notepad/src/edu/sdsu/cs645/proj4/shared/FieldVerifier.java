package edu.sdsu.cs645.proj4.shared;

/**
 * <p>
 * FieldVerifier validates that the name the user enters is valid.
 * </p>
 * <p>
 * This class is in the <code>shared</code> package because we use it in both
 * the client code and on the server. On the client, we verify that the name is
 * valid before sending an RPC request so the user doesn't have to wait for a
 * network round trip to get feedback. On the server, we verify that the name is
 * correct to ensure that the input is correct regardless of where the RPC
 * originates.
 * </p>
 * <p>
 * When creating a class that is used on both the client and the server, be sure
 * that all code is translatable and does not use native JavaScript. Code that
 * is not translatable (such as code that interacts with a database or the file
 * system) cannot be compiled into client-side JavaScript. Code that uses native
 * JavaScript (such as Widgets) cannot be run on the server.
 * </p>
 */
public class FieldVerifier {

	/**
	 * validates username entered by user
	 * 
	 * @param username
	 *            entered by user
	 * @return true if valid, otherwise false
	 */
	public static boolean isValidUserName(String username) {
		if (username == null) {
			return false;
		}
		return username.length() > 3;
	}

	/**
	 * validates password entered by user
	 * 
	 * @param password
	 *            entered by user
	 * @return true if valid, otherwise false
	 */
	public static boolean isValidPassword(String password) {
		if (password == null) {
			return false;
		}
		return password.length() > 3;
	}
}
