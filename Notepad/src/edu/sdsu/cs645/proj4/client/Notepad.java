package edu.sdsu.cs645.proj4.client;

import java.util.ArrayList;

import edu.sdsu.cs645.proj4.shared.Conversion;
import edu.sdsu.cs645.proj4.shared.FieldVerifier;
import edu.sdsu.cs645.proj4.shared.StatusCode;

import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.core.client.GWT;
import com.google.gwt.core.client.Scheduler;
import com.google.gwt.dom.client.Style.Cursor;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.KeyCodes;
import com.google.gwt.event.dom.client.KeyUpEvent;
import com.google.gwt.event.dom.client.KeyUpHandler;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Anchor;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.DialogBox;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Grid;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.Hidden;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.PasswordTextBox;
import com.google.gwt.user.client.ui.RichTextArea;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.VerticalPanel;

/**
 * Entry point classes define <code>onModuleLoad()</code>.
 */
public class Notepad implements EntryPoint {
	private final NotepadServiceAsync notepadService = GWT
			.create(NotepadService.class);
	private FlowPanel panelNote = null;
	private RichTextArea textAreaNote = null;
	private HTML htmlNote = null;
	private HTML htmlStatus = null;
	private HTML htmlConfirmMsg = null;
	private FlowPanel panelFiles = null;
	private Hidden hiddenPath = null;
	private Hidden hiddenFilename = null;
	private boolean showConfirmMsg = false;

	public void onModuleLoad() {

		AsyncCallback callback = new AsyncCallback() {
			@Override
			public void onSuccess(Object o) {
				if ((Boolean) o) {
					hiddenPath = new Hidden();
					hiddenPath.setValue("/");
					filesPanel();
				} else {
					loadLoginPanel();
				}
			}

			@Override
			public void onFailure(Throwable caught) {
				// TODO Auto-generated method stub

			}
		};
		notepadService.isAuthorized(callback);
	}

	/**
	 * helper method to load login panel
	 */
	private void loadLoginPanel() {
		FlowPanel p = buildLoginPanel();
		RootPanel.get().clear();
		RootPanel.get().add(p);
	}

	/**
	 * helper method to load user panel with notepad
	 */
	private void loadUserPanel() {
		FlowPanel p = buildUserPanel();
		RootPanel.get().clear();
		RootPanel.get().add(p);
	}

	/**
	 * helper method to load login panel
	 */
	private void loadAddFilePanel() {
		FlowPanel p = buildAddFilePanel();
		RootPanel.get().clear();
		RootPanel.get().add(p);
	}

	/**
	 * helper method to load user panel with notepad
	 */
	private void loadNotePanel() {
		FlowPanel p = buildNotePanel();
		RootPanel.get().clear();
		RootPanel.get().add(p);
		loadPanel();
	}

	/**
	 * build the html content of the user panel
	 * 
	 * @return user panel
	 */
	private FlowPanel buildUserPanel() {
		FlowPanel main = new FlowPanel();
		main.add(new HTML("<h1>Dashboard</h1>"));

		if (showConfirmMsg) {
			main.add(htmlConfirmMsg);
			showConfirmMsg = false;
		}

		FlowPanel buttonPanel = new FlowPanel();
		buttonPanel.setStyleName("buttonPanel");

		Button addFileButton = new Button("Add File");
		// Button addFolderButton = new Button("Add File");
		Button logoutButton = new Button("Logout");

		addFileButton.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent e) {
				loadAddFilePanel();
			}
		});
		// addFolderButton.addClickHandler(new ClickHandler() {
		// public void onClick(ClickEvent e) {
		// savePanel();
		// }
		// });
		logoutButton.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent e) {
				logout();
			}
		});

		buttonPanel.add(addFileButton);
		// buttonPanel.add(addFolderButton);
		buttonPanel.add(logoutButton);
		main.add(buttonPanel);

		main.add(hiddenPath);

		main.add(new HTML("<h3>Browse Saved Files</h3>"));

		main.add(panelFiles);

		main.setStyleName("main");

		return main;
	}

	/**
	 * build the html content of the add a file panel
	 * 
	 * @return add a file panel
	 */
	private FlowPanel buildAddFilePanel() {

		FlowPanel main = new FlowPanel();

		main.add(new HTML("<h1>Add a Note</h1>"));

		FlowPanel buttonPanel = new FlowPanel();
		buttonPanel.setStyleName("buttonPanel");
		Button logoutButton = new Button("Logout");
		logoutButton.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent e) {
				logout();
			}
		});
		buttonPanel.add(logoutButton);
		main.add(buttonPanel);

		FlowPanel panel = new FlowPanel();

		Grid g = new Grid(1, 3);

		g.setWidget(0, 0, new Label("Note:"));
		final TextBox filenameTextBox = new TextBox();
		filenameTextBox.addKeyUpHandler(new KeyUpHandler() {
			@Override
			public void onKeyUp(KeyUpEvent event) {
				if (event.getNativeKeyCode() == KeyCodes.KEY_ENTER) {
					addFilePanel(hiddenPath.getValue(),
							filenameTextBox.getText());
				}
			}
		});
		g.setWidget(0, 1, filenameTextBox);
		Button createFileButton = new Button("Create");
		g.setWidget(0, 2, createFileButton);

		createFileButton.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent e) {
				addFilePanel(hiddenPath.getValue(), filenameTextBox.getText());
			}
		});

		panel.add(g);
		panel.setStyleName("login");

		htmlStatus = new HTML();
		htmlStatus.setStyleName("status");

		panel.add(htmlStatus);

		main.add(panel);

		main.setStyleName("main");

		// set focus
		Scheduler.get().scheduleDeferred(new Scheduler.ScheduledCommand() {
			public void execute() {
				filenameTextBox.setFocus(true);
			}
		});

		return main;
	}

	/**
	 * build the html content of a note panel
	 * 
	 * @return user panel
	 */
	private FlowPanel buildNotePanel() {
		FlowPanel main = new FlowPanel();
		main.add(new HTML("<h1>Note: "
				+ hiddenFilename.getValue().toUpperCase() + "</h1>"));

		if (showConfirmMsg) {
			main.add(htmlConfirmMsg);
			showConfirmMsg = false;
		}

		FlowPanel buttonPanel = new FlowPanel();
		buttonPanel.setStyleName("buttonPanel");

		Button backButton = new Button("Go Back");
		Button editButton = new Button("Edit");
		Button loadButton = new Button("Restore");
		Button storeButton = new Button("Save");
		Button logoutButton = new Button("Logout");

		backButton.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent e) {
				// reload files
				panelFiles.clear();
				filesPanel();
			}
		});
		editButton.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent e) {
				panelNote.setStyleName("editNotePanel");
				// set focus
				Scheduler.get().scheduleDeferred(
						new Scheduler.ScheduledCommand() {
							public void execute() {
								textAreaNote.setFocus(true);
							}
						});

			}
		});
		loadButton.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent e) {
				loadPanel();
			}
		});
		storeButton.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent e) {
				savePanel();
			}
		});
		logoutButton.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent e) {
				logout();
			}
		});

		buttonPanel.add(backButton);
		buttonPanel.add(editButton);
		buttonPanel.add(loadButton);
		buttonPanel.add(storeButton);
		buttonPanel.add(logoutButton);
		main.add(buttonPanel);

		panelNote = new FlowPanel();
		textAreaNote = new RichTextArea();
		htmlNote = new HTML();
		panelNote.add(textAreaNote);
		panelNote.add(htmlNote);
		panelNote.setStyleName("viewNotePanel");
		main.add(panelNote);

		htmlStatus = new HTML();
		htmlStatus.setStyleName("status");

		main.add(htmlStatus);

		Image deleteIcon = new Image();
		deleteIcon.setUrl("img/delete.png");
		deleteIcon.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent e) {
				deletePanel();
			}
		});
		deleteIcon.getElement().getStyle().setCursor(Cursor.POINTER);
		Anchor deleteLink = new Anchor();
		deleteLink.setHTML("Delete this note");
		deleteLink.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent e) {
				deletePanel();
			}
		});
		deleteLink.getElement().getStyle().setCursor(Cursor.POINTER);

		FlowPanel deletePanel = new FlowPanel();
		deletePanel.add(deleteIcon);
		deletePanel.add(deleteLink);
		main.add(deletePanel);

		main.add(hiddenPath);
		main.add(hiddenFilename);

		main.setStyleName("main");

		return main;
	}

	/**
	 * builds the html content of the login panel
	 * 
	 * @return login panel
	 */
	private FlowPanel buildLoginPanel() {

		FlowPanel main = new FlowPanel();

		main.add(new HTML("<h1>Login to Online Notepad</h1>"));

		FlowPanel panel = new FlowPanel();

		Grid g = new Grid(3, 2);

		g.setWidget(0, 0, new Label("Username:"));
		final TextBox usernameTextBox = new TextBox();
		g.setWidget(0, 1, usernameTextBox);
		g.setWidget(1, 0, new Label("Password:"));
		final PasswordTextBox passwordTextBox = new PasswordTextBox();
		g.setWidget(1, 1, passwordTextBox);

		usernameTextBox.addKeyUpHandler(new KeyUpHandler() {
			@Override
			public void onKeyUp(KeyUpEvent event) {
				if (event.getNativeKeyCode() == KeyCodes.KEY_ENTER) {
					loginPanel(usernameTextBox.getText(),
							passwordTextBox.getText());
				}
			}
		});
		passwordTextBox.addKeyUpHandler(new KeyUpHandler() {
			@Override
			public void onKeyUp(KeyUpEvent event) {
				if (event.getNativeKeyCode() == KeyCodes.KEY_ENTER) {
					loginPanel(usernameTextBox.getText(),
							passwordTextBox.getText());
				}
			}
		});

		Button clear = new Button("Clear");
		Button submit = new Button("Login");
		g.setWidget(2, 0, clear);
		g.setWidget(2, 1, submit);

		clear.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent e) {
				usernameTextBox.setText("");
				passwordTextBox.setText("");
			}
		});
		submit.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent e) {
				loginPanel(usernameTextBox.getText(), passwordTextBox.getText());
			}
		});

		panel.add(g);
		panel.setStyleName("login");

		htmlStatus = new HTML();
		htmlStatus.setStyleName("status");

		panel.add(htmlStatus);

		main.add(panel);

		main.setStyleName("main");

		// set focus
		Scheduler.get().scheduleDeferred(new Scheduler.ScheduledCommand() {
			public void execute() {
				usernameTextBox.setFocus(true);
			}
		});

		return main;
	}

	/**
	 * Clear cookies/sessions/files and redirect user back to login
	 */
	private void logout() {
		AsyncCallback callback = new AsyncCallback() {
			@Override
			public void onSuccess(Object o) {
				if ((Boolean) o) {
					panelFiles.clear();
					loadLoginPanel();
				} else {
					htmlStatus.setText("FAILED");
				}
			}

			@Override
			public void onFailure(Throwable caught) {
				htmlStatus.setText("Failed to logout!");
			}
		};

		notepadService.logout(callback);
	}

	/**
	 * Asynchronously load the login panel
	 * 
	 * @param username
	 *            user's entered username
	 * @param password
	 *            user's entered password
	 */
	private void loginPanel(String username, String password) {
		AsyncCallback callback = new AsyncCallback() {
			@Override
			public void onSuccess(Object o) {
				if ((Boolean) o) {
					hiddenPath = new Hidden();
					hiddenPath.setValue("/");
					filesPanel();
				} else {
					htmlStatus.setText("FAILED");
				}
			}

			@Override
			public void onFailure(Throwable caught) {
				htmlStatus.setText("Failed to login!");
			}
		};

		notepadService.login(username, password, callback);
	}

	/**
	 * Asynchronously load panel of files
	 */
	private void filesPanel() {
		AsyncCallback callback = new AsyncCallback() {
			@Override
			public void onSuccess(Object o) {
				if (o != null) {
					if (panelFiles == null) {
						panelFiles = new FlowPanel();
						panelFiles.setStyleName("filesPanel");
					}
					for (final String filename : (ArrayList<String>) o) {
						Button fileButton = new Button(filename);
						fileButton.setStyleName("file");
						fileButton.addClickHandler(new ClickHandler() {
							public void onClick(ClickEvent e) {
								if (hiddenFilename == null)
									hiddenFilename = new Hidden();
								hiddenFilename.setValue(filename);
								loadNotePanel();
							}
						});
						panelFiles.add(fileButton);
					}
				}
				loadUserPanel();
			}

			@Override
			public void onFailure(Throwable caught) {
				htmlStatus.setText("Failed to create note!");
			}
		};

		notepadService.getFiles(hiddenPath.getValue(), callback);
	}

	/**
	 * Asynchronously load screen to add a file
	 * 
	 * @param path
	 *            folder path of new file
	 * @param filename
	 *            name of new file
	 */
	private void addFilePanel(String path, final String filename) {
		AsyncCallback callback = new AsyncCallback() {
			@Override
			public void onSuccess(Object o) {
				if (((Integer) o) == StatusCode.SUCCESS) {
					if (hiddenFilename == null) {
						hiddenFilename = new Hidden();
					}
					hiddenFilename.setValue(filename);
					if (htmlConfirmMsg == null) {
						htmlConfirmMsg = new HTML();
					}
					htmlConfirmMsg.setHTML("Note " + filename
							+ " has been successfully created");
					htmlConfirmMsg.setStyleName("confirmation");
					showConfirmMsg = true;
					loadNotePanel();
				} else {
					htmlStatus.setText("FAILED");
				}
			}

			@Override
			public void onFailure(Throwable caught) {
				htmlStatus.setText("Failed to create note!");
			}
		};

		notepadService.addFile(path, filename, callback);
	}

	/**
	 * Asynchronously load contents from file into text area
	 */
	private void loadPanel() {
		AsyncCallback callback = new AsyncCallback() {
			@Override
			public void onSuccess(Object o) {
				textAreaNote.setHTML(o + "");
				htmlNote.setHTML(Conversion.toHtml(o + ""));
				panelNote.setStyleName("viewNotePanel");
				htmlStatus.setText("Notepad restored to last saved");
			}

			@Override
			public void onFailure(Throwable caught) {
				htmlStatus.setText("Notepad failed to load!");
			}
		};

		notepadService.load(hiddenPath.getValue(), hiddenFilename.getValue(),
				callback);
	}

	/**
	 * Asynchronously save contents in text area to file
	 */
	private void savePanel() {
		AsyncCallback callback = new AsyncCallback() {
			@Override
			public void onSuccess(Object o) {
				htmlNote.setHTML(Conversion.toHtml(textAreaNote.getText()));
				panelNote.setStyleName("viewNotePanel");
				htmlStatus.setText("Notepad contents saved");
			}

			@Override
			public void onFailure(Throwable caught) {
				htmlStatus.setText("Notepad failed to save!");
			}
		};

		notepadService.store(hiddenPath.getValue(), hiddenFilename.getValue(),
				textAreaNote.getText(), callback);
	}

	/**
	 * Asynchronously save contents in text area to file
	 */
	private void deletePanel() {
		AsyncCallback callback = new AsyncCallback() {
			@Override
			public void onSuccess(Object o) {
				if (htmlConfirmMsg == null)
					htmlConfirmMsg = new HTML();
				htmlConfirmMsg.setHTML("File " + hiddenFilename.getValue()
						+ " has been successfully deleted");
				htmlConfirmMsg.setStyleName("confirmation");
				showConfirmMsg = true;
				// reload files
				panelFiles.clear();
				filesPanel();
			}

			@Override
			public void onFailure(Throwable caught) {
				htmlStatus.setText("Failed to delete note!");
			}
		};

		notepadService.deleteFile(hiddenPath.getValue(),
				hiddenFilename.getValue(), callback);
		;
	}

}
