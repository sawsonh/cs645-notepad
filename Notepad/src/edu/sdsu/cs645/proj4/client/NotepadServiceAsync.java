package edu.sdsu.cs645.proj4.client;

import java.util.ArrayList;

import com.google.gwt.user.client.rpc.AsyncCallback;

/**
 * The async counterpart of <code>NotepadService</code>.
 */
public interface NotepadServiceAsync {
	void isAuthorized(AsyncCallback<Boolean> callback)
			throws IllegalArgumentException;

	void login(String username, String password, AsyncCallback<Boolean> callback)
			throws IllegalArgumentException;

	void logout(AsyncCallback<Boolean> callback)
			throws IllegalArgumentException;

	void getFiles(String path, AsyncCallback<ArrayList<String>> callback)
			throws IllegalArgumentException;

	void getFolders(String path, AsyncCallback<ArrayList<String>> callback)
			throws IllegalArgumentException;

	void addFile(String path, String filename, AsyncCallback<Integer> callback)
			throws IllegalArgumentException;

	void deleteFile(String path, String filename,
			AsyncCallback<Integer> callback) throws IllegalArgumentException;

	void addFolder(String path, String foldername,
			AsyncCallback<Integer> callback) throws IllegalArgumentException;

	void load(String path, String filename, AsyncCallback<String> callback)
			throws IllegalArgumentException;

	void store(String path, String filename, String content,
			AsyncCallback callback) throws IllegalArgumentException;
}
