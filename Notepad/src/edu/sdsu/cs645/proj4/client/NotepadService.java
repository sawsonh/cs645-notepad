package edu.sdsu.cs645.proj4.client;

import java.util.ArrayList;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;

/**
 * The client-side stub for the RPC service.
 */
@RemoteServiceRelativePath("notepad")
public interface NotepadService extends RemoteService {
	Boolean isAuthorized() throws IllegalArgumentException;

	Boolean login(String username, String password)
			throws IllegalArgumentException;

	Boolean logout() throws IllegalArgumentException;

	ArrayList<String> getFiles(String path) throws IllegalArgumentException;

	ArrayList<String> getFolders(String path) throws IllegalArgumentException;

	Integer addFile(String path, String filename)
			throws IllegalArgumentException;

	Integer deleteFile(String path, String filename)
			throws IllegalArgumentException;

	Integer addFolder(String path, String foldername)
			throws IllegalArgumentException;

	String load(String path, String filename) throws IllegalArgumentException;

	void store(String path, String filename, String content)
			throws IllegalArgumentException;
}
